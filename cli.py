#!/usr/bin/env python
import click
from src import make_game


@click.command()
@click.argument("file", type=click.File("rb"))
def run(file):
    raw_moves = file.read()
    end_state = make_game(raw_moves)
    click.echo(end_state)


if __name__ == "__main__":
    run()
