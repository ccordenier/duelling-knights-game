# Installation

To run this you will need to have python 2.7 installed along with virtualenv and pip. To install the project, first create a virtual environment:

    virtualenv .venv

Then activate the virtual environment:

    source .venv/bin/activate

Once the virtual environment is activated, you can install the dependencies:

    pip install -r requirements.txt


# Running the game

You can run the game with the following command:

    cli.py <file_path>


# Running the tests

The tests can be run using:

    ptw
