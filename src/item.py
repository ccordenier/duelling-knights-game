class WeaponModifiers(object):
    def __init__(self, attack, defence):
        self.attack = attack
        self.defence = defence


class Item(object):
    state = "UNEQUIPPED"

    def __init__(
        self,
        name,
        modifiers,
        position=None
    ):
        self.name = name
        self.modifiers = modifiers
        self.position = position

    @property
    def attack_modifier(self):
        return self.modifiers.attack

    @property
    def defence_modifier(self):
        return self.modifiers.defence

    def get_state(self):
        return [
            self.position.position, True if self.state is "EQUIPPED" else False
        ]


class EquippedItem(Item):
    state = "EQUIPPED"


class Fists(Item):
    name = "Fists"
    modifiers = WeaponModifiers(attack=0, defence=0)

    def __init__(self, position):
        self.position = position
