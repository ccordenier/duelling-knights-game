class BoardPosition(object):
    def __init__(self, y, x):
        self.y = y
        self.x = x

    @property
    def position(self):
        return (self.y, self.x)

    def get_new(self, direction):
        direction_map = {
            "N": (-1, 0),
            "S": (1, 0),
            "E": (0, 1),
            "W": (0, -1)
        }
        movement_y, movement_x = direction_map[direction]

        return BoardPosition(
            movement_y + self.y,
            movement_x + self.x
        )

    def is_valid(self):
        return (
            self.x >= 0 and
            self.x < 8 and
            self.y >= 0 and
            self.y < 8
        )

    def __eq__(self, other_board_position):
        return self.position == other_board_position

    def __repr__(self):
        return "BoardPosition<{y}, {x}>".format(y=self.y, x=self.x)
