class Board(object):
    def __init__(self, knights, items):
        self.knights = {
            knight.name: knight for knight in knights
        }

        self.items = {
            item.name: item for item in items
        }

    def get_name(self, abbreviation):
        for name in self.knights.keys():
            if name.startswith(abbreviation.lower()):
                return name
        raise ValueError("No name found for abbreviation")

    def get_knight(self, name):
        return self.knights[name]

    def get_other_knights(self, name):
        return [
            k for k in self.knights.values()
            if k.name is not name
        ]

    def get_unequipped_items(self):
        return [
            i for i in self.items.values() if i.state is not "EQUIPPED"
        ]

    def update_knight(self, knight):
        self.knights[knight.name] = knight

    def get_item(self, name):
        return self.items[name]

    def update_item(self, item):
        # Handle case where we would update with "Fists"
        if item.name not in self.items.keys():
            return
        self.items[item.name] = item

    def execute(self, moves):
        for abbreviated_name, direction in moves:
            knight_name = self.get_name(abbreviated_name)
            knight = self.get_knight(knight_name)
            other_knights = self.get_other_knights(knight_name)

            knight, dropped_item = knight.move(direction)
            self.update_knight(knight)
            self.update_item(dropped_item)

            unequipped_items = self.get_unequipped_items()
            for item in unequipped_items:
                if knight.encounters(item):
                    knight, item = knight.equips(item)
                    self.update_knight(knight)
                    self.update_item(item)

            for other_knight in other_knights:
                if knight.encounters(other_knight):
                    knight, other_knight, dropped_item = knight.fights(other_knight)
                    self.update_knight(knight)
                    self.update_knight(other_knight)
                    self.update_item(dropped_item)

    def get_state(self):
        state = {}
        for knight in self.knights.values():
            state[knight.name] = knight.get_state()
        for item in self.items.values():
            state[item.name] = item.get_state()
        return state
