import pytest
from game import (
    Board,
    WeaponModifiers,
    Item,
    DrownedKnight,
    DeadKnight,
    Knight,
    BoardPosition,
    make_game
)


@pytest.fixture
def knight():
    return Knight("ivanhoe", BoardPosition(7, 0))


@pytest.fixture
def drowned_knight():
    return DrownedKnight("lancelot", BoardPosition(9, 0))


@pytest.fixture
def dead_knight():
    return DeadKnight("king arthur", BoardPosition(6, 0))


@pytest.fixture
def strong_knight():
    position = BoardPosition(6, 0)
    staff = Item("Staff", WeaponModifiers(2, 2), position)
    return Knight(
        "hulk knight",
        position,
        staff
    )


class TestKnightMovement(object):
    def test_it_updates_the_coordinates_after_a_move(self, knight):
        knight, _ = knight.move("N")
        assert knight.position == BoardPosition(6, 0)

        knight, _ = knight.move("S")
        assert knight.position == BoardPosition(7, 0)

        knight, _ = knight.move("E")
        assert knight.position == BoardPosition(7, 1)

        knight, _ = knight.move("W")
        assert knight.position == BoardPosition(7, 0)

    def test_the_knight_drowns_if_it_moves_off_the_board(self, knight):
        knight, _ = knight.move("S")
        assert knight.state == "DROWNED"

        knight = Knight("Can't Swim Knight", BoardPosition(0, 0))
        knight, _ = knight.move("W")
        assert knight.state == "DROWNED"

    def test_the_position_of_a_drowned_knight_is_None(self, knight):
        knight, _ = knight.move("S")
        assert knight.state == "DROWNED"
        assert knight.position is None

    def test_a_drowned_knight_does_not_move(self, knight):
        drowned_knight, _ = knight.move("S")
        drowned_knight_after_move, _ = drowned_knight.move("S")
        assert drowned_knight_after_move == drowned_knight

    def test_a_dead_knight_does_not_move(self, dead_knight):
        dead_knight_after_move, _ = dead_knight.move("S")
        assert dead_knight_after_move == dead_knight

    def test_the_position_of_a_dead_knight_is_where_it_died(self, dead_knight):
        assert dead_knight.position == BoardPosition(6, 0)


class TestKnightEncounters(object):
    def test_a_live_knight_has_base_defence_and_attack(self, knight):
        assert knight.base_defence == 1
        assert knight.base_attack == 1

    def test_a_drowned_knight_has_no_base_attack_or_base_defence(
        self, drowned_knight
    ):
        assert drowned_knight.base_defence == 0
        assert drowned_knight.base_attack == 0

    def test_the_attack_of_a_knight_is_not_modified_by_the_surprise_bonus(
        self, knight
    ):
        assert knight.attack == 1

    def test_the_attack_of_a_knight_is_modified_by_any_items_equipped(
        self, knight, item
    ):
        knight, _ = knight.equips(item)
        assert knight.attack == 2

    def test_the_defence_of_a_knight_is_modified_by_items_equipped(
        self, knight, item
    ):
        knight, _ = knight.equips(item)
        assert knight.defence == 2

    def test_a_drowned_knight_has_no_attack_or_defence(
        self, drowned_knight
    ):
        assert drowned_knight.attack == 0
        assert drowned_knight.defence == 0

    def test_a_dead_knight_has_no_attack_or_defence(
        self, dead_knight
    ):
        assert dead_knight.attack == 0
        assert dead_knight.defence == 0

    def test_a_drowned_knight_cannot_equip_a_weapon(
        self, drowned_knight, item
    ):
        drowned_knight_after_equip, _ = drowned_knight.equips(item)
        assert drowned_knight_after_equip == drowned_knight

    def test_a_dead_knight_cannot_equip_a_weapon(
        self, dead_knight, item
    ):
        dead_knight_after_equipment, _ = dead_knight.equips(item)
        assert dead_knight_after_equipment == dead_knight

    def test_in_a_fight_the_knight_with_the_highest_score_lives(
        self, strong_knight, knight
    ):
        strong_knight, knight, _ = strong_knight.fights(knight)
        assert strong_knight.state == "LIVE"
        assert knight.state == "DEAD"

    def test_in_a_fight_where_the_attack_and_defence_are_equal_the_attack_wins(
        self, knight
    ):
        other_knight = Knight("Other Knight", BoardPosition(0, 0))
        assert knight.attack == other_knight.defence
        knight, other_knight, _ = knight.fights(other_knight)
        assert knight.state == "LIVE"
        assert other_knight.state == "DEAD"

    def test_dead_knights_lose_equipped_items(
        self, knight, strong_knight, item
    ):
        knight, _ = knight.equips(item)
        assert knight.has_item() is True
        strong_knight, knight, _ = strong_knight.fights(knight)
        assert knight.state == "DEAD"
        assert knight.has_item() is False

    def test_fighting_a_dead_knight_changes_nothing(
        self, dead_knight, knight
    ):
        dead_knight_after_fight, knight_after_fight, _ = dead_knight.fights(knight)
        assert dead_knight == dead_knight_after_fight
        assert knight == knight_after_fight

    def test_fighting_a_drowned_knight_changes_nothnig(
        self, drowned_knight, knight
    ):
        drowned_knight_after_fight, knight_after_fight, _ = drowned_knight.fights(knight)
        assert drowned_knight == drowned_knight_after_fight
        assert knight == knight_after_fight


@pytest.fixture()
def item():
    return Item(
        name="Staff",
        modifiers=WeaponModifiers(attack=1, defence=1),
        position=BoardPosition(0, 7)
    )


class TestItems(object):
    def test_items_have_an_attack_and_defence_modifier(self, item):
        assert item.attack_modifier == 1
        assert item.defence_modifier == 1

    def test_items_have_a_position(self, item):
        assert item.position == BoardPosition(0, 7)

    def test_when_a_knight_dies_they_drop_the_item_on_the_position_they_died(
        self, strong_knight, knight, item
    ):
        last_position = knight.position
        knight, _ = knight.equips(item)
        strong_knight, knight, dropped_item = strong_knight.fights(knight)
        assert knight.state == "DEAD"
        assert knight.has_item() is False
        assert dropped_item.position == last_position

    def test_when_a_knight_drowns_they_drop_the_item_on_their_last_position(
        self, knight, item
    ):
        last_position = knight.position
        knight.equips(item)
        knight, item = knight.move("S")
        assert knight.state == "DROWNED"
        assert item.position == last_position

    def test_a_knight_with_an_item_cannot_pick_up_another(
        self, knight, item
    ):
        another_item = Item(
            "Lightsaber",
            WeaponModifiers(attack=1, defence=1),
            position=BoardPosition(0, 7)
        )
        knight, _ = knight.equips(item)
        knight_after_equipping_again, _ = knight.equips(another_item)
        assert knight is knight_after_equipping_again

    def test_dead_or_drowned_knights_cannot_equip(
        self, dead_knight, drowned_knight, item
    ):
        dead_knight_after_equipment, _ = dead_knight.equips(item)
        assert dead_knight_after_equipment is dead_knight

        drowned_knight_after_equipment, _ = drowned_knight.equips(item)
        assert drowned_knight_after_equipment is drowned_knight

    def test_when_an_item_is_equipped_its_state_is_equipped(
        self, knight, item
    ):
        knight, item = knight.equips(item)
        assert item.state == "EQUIPPED"


class TestBoard(object):
    def test_a_board_contains_the_knights_on_the_board(
        self, knight
    ):
        board = Board(knights=[knight], items=[])
        assert board.get_knight("ivanhoe") is knight

        with pytest.raises(KeyError):
            board.get_knight("Doesn't Exist")

    def test_the_state_of_the_knight_on_the_board_can_be_updated(
        self, knight
    ):
        board = Board(knights=[knight], items=[])
        assert board.get_knight("ivanhoe") is knight

        knight, _ = knight.move("S")
        board.update_knight(knight)
        drowned_knight = board.get_knight("ivanhoe")
        assert knight is drowned_knight

    def test_the_board_contains_the_items_on_the_board(
        self, knight, item
    ):
        board = Board(knights=[knight], items=[item])
        board.get_item("Staff")

    def test_the_state_of_each_item_on_the_board_can_be_updated(
        self, knight, item
    ):
        board = Board(knights=[knight], items=[item])
        assert board.get_item(item.name) is item
        knight, item = knight.equips(item)
        board.update_item(item)
        assert board.get_item(item.name).state is "EQUIPPED"


class TestGameLoop(object):
    def test_it_should_update_the_state_of_the_board_according_to_the_moves(
        self
    ):
        r_knight = Knight("red", BoardPosition(0, 0))
        board = Board(knights=[r_knight], items=[])
        moves = [("R", "S"), ("R", "S")]
        board.execute(moves)

        end_state = {
            "red": [(2, 0), "LIVE", None, 1, 1]
        }
        assert board.get_state() == end_state

    def test_it_should_execute_fights_if_two_knights_are_on_the_same_position(
        self, knight, strong_knight
    ):
        board = Board(knights=[knight, strong_knight], items=[])
        moves = [("I", "N")]
        board.execute(moves)

        end_state = {
            "ivanhoe": [(6, 0), "DEAD", None, 0, 0],
            "hulk knight": [(6, 0), "LIVE", "Staff", 3, 3]
        }
        assert board.get_state() == end_state

    def test_knights_should_pick_up_weapons_on_the_same_position(
        self, item
    ):
        r_knight = Knight("red", BoardPosition(0, 6))
        board = Board(knights=[r_knight], items=[item])
        moves = [("R", "E")]
        board.execute(moves)

        end_state = {
            "red": [(0, 7), "LIVE", "Staff", 2, 2],
            "Staff": [(0, 7), True]
        }
        assert board.get_state() == end_state


class TestFullGame(object):
    def test_the_game_should_result_in_the_state_in_the_readme(
        self
    ):
        red = Knight("red", BoardPosition(0, 0))
        blue = Knight("blue", BoardPosition(7, 0))
        green = Knight("green", BoardPosition(7, 7))
        yellow = Knight("yellow", BoardPosition(0, 7))

        axe = Item("axe", WeaponModifiers(2, 0), BoardPosition(2, 2))
        dagger = Item("dagger", WeaponModifiers(1, 0), BoardPosition(2, 5))
        helmet = Item("helmet", WeaponModifiers(0, 1), BoardPosition(5, 5))
        magic_staff = Item(
            "magic_staff", WeaponModifiers(1, 1), BoardPosition(5, 2))

        board = Board(
            knights=[red, blue, green, yellow],
            items=[axe, dagger, helmet, magic_staff]
        )
        moves = [("R", "S"), ("R", "S"), ("B", "E"), ("G", "N"), ("Y", "N")]
        board.execute(moves)

        end_state = {
            "red": [(2, 0), "LIVE", None, 1, 1],
            "blue": [(7, 1), "LIVE", None, 1, 1],
            "green": [(6, 7), "LIVE", None, 1, 1],
            "yellow": [None, "DROWNED", None, 0, 0],
            "axe": [(2, 2), False],
            "magic_staff": [(5, 2), False],
            "helmet": [(5, 5), False],
            "dagger": [(2, 5), False]
        }
        assert board.get_state() == end_state

    @pytest.mark.parametrize("raw_moves", [
        "R-S\nY-N",
        "GAME-START\nY:S",
        "R:S B:E",
    ])
    def test_it_should_raise_an_exception_if_the_moves_are_not_valid(
        self, raw_moves
    ):
        with pytest.raises(ValueError):
            make_game(raw_moves)
