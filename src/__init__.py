import re

from knight import Knight
from position import BoardPosition
from board import Board
from item import Item, WeaponModifiers


def setup_board():
        red = Knight("red", BoardPosition(0, 0))
        blue = Knight("blue", BoardPosition(7, 0))
        green = Knight("green", BoardPosition(7, 7))
        yellow = Knight("yellow", BoardPosition(0, 7))

        axe = Item("axe", WeaponModifiers(2, 0), BoardPosition(2, 2))
        dagger = Item("dagger", WeaponModifiers(1, 0), BoardPosition(2, 5))
        helmet = Item("helmet", WeaponModifiers(0, 1), BoardPosition(5, 5))
        magic_staff = Item(
            "magic_staff", WeaponModifiers(1, 1), BoardPosition(5, 2))

        board = Board(
            knights=[red, blue, green, yellow],
            items=[axe, dagger, helmet, magic_staff]
        )
        return board


def is_valid(raw_moves):
    return re.match(
        "^GAME-START\n((R|B|G|Y):(N|E|S|W)\n)+GAME-END$",
        raw_moves
    )


def parse_moves(raw_moves):
    moves_without_header_and_footer = raw_moves.split("\n")[1:-2]
    return [move.split(":") for move in moves_without_header_and_footer]


def make_game(raw_moves):
    if not is_valid(raw_moves):
        raise ValueError("The format of the moves is invalid")

    parsed_moves = parse_moves(raw_moves)

    board = setup_board()
    board.execute(parsed_moves)
    return board.get_state()


__all__ = ["make_game"]
