from item import Fists, Item, EquippedItem


class BaseKnight(object):
    """
    An abstract class for all knights
    """
    def __init__(self, name, position, item=None):
        self.name = name
        self._position = position
        self._item = item or Fists(position)

    def has_item(self):
        return (
            self._item.attack_modifier is not 0 and
            self._item.defence_modifier is not 0
        )

    @property
    def attack(self):
        return (
            self.base_attack +
            self._item.attack_modifier
        )

    @property
    def defence(self):
        return (
            self.base_defence +
            self._item.defence_modifier
        )

    def encounters(self, item_or_knight):
        return self.position == item_or_knight.position

    def get_state(self):
        return [
            self.position.position if self.position else self.position,
            self.state,
            self._item.name if self.has_item() else None,
            self.attack,
            self.defence
        ]


class IncapacitatedKnight(BaseKnight):
    """
    An abstract class for drowned and dead knights
    """
    state = "INCAPACITATED"
    base_defence = 0
    base_attack = 0

    def move(self, direction):
        return self, Fists(self.position)

    def equips(self, item):
        return self, Fists(self.position)

    def fights(self, defending_knight):
        return (
            self,
            defending_knight,
            Fists(self.position)
        )


class DrownedKnight(IncapacitatedKnight):
    state = "DROWNED"

    @property
    def position(self):
        return None


class DeadKnight(IncapacitatedKnight):
    state = "DEAD"

    @property
    def position(self):
        return self._position


class Knight(BaseKnight):
    state = "LIVE"
    base_defence = 1
    base_attack = 1
    surprise_bonus = 0.5

    @property
    def position(self):
        return self._position

    def move(self, direction):
        new_position = self.position.get_new(direction)
        if new_position.is_valid():
            item = Item(
                name=self._item.name,
                modifiers=self._item.modifiers,
                position=new_position)
            return (
                Knight(
                    name=self.name,
                    position=new_position),
                item)
        dropped_item = Item(
            name=self._item.name,
            modifiers=self._item.modifiers,
            position=self._position
        )
        return (
            DrownedKnight(name=self.name, position=new_position),
            dropped_item
        )

    def equips(self, item):
        if self.has_item():
            return self, item

        equipped_item = EquippedItem(
            name=item.name,
            modifiers=item.modifiers,
            position=self.position
        )
        return (
            Knight(name=self.name, position=self.position, item=equipped_item),
            equipped_item
        )

    def fights(self, defending_knight):
        if self.attack + self.surprise_bonus > defending_knight.defence:
            dropped_item = Item(
                name=defending_knight._item.name,
                modifiers=defending_knight._item.modifiers,
                position=defending_knight._position
            )
            return (
                self,
                DeadKnight(
                    name=defending_knight.name,
                    position=defending_knight._position
                ),
                dropped_item
            )
        else:
            dropped_item = Item(
                name=self._item.name,
                modifiers=self._item.modifiers,
                position=self._position
            )
            return (
                DeadKnight(
                    name=self.name,
                    position=self._position),
                defending_knight,
                dropped_item,
            )
